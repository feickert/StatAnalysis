Boost C++ Libraries
===================

This package is used to build Boost for the standalone analysis
release.

The package only builds Boost if the `ATLAS_BUILD_BOOST` (cache)
variable is set to `TRUE`. It's the responsibility of the project
building this "package" to set a correct value for that variable,
allowing the user to override it from the command line if necessary.
